//
//  Article.swift
//  NextRadioTvTest
//
//  Created by Douik Mohamed Houcem on 18/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import RealmSwift

class Article: Object {
    
    @objc dynamic var article = 0
    @objc dynamic var chapo = ""
    @objc dynamic var comments = 0
    @objc dynamic var content = ""
    @objc dynamic var date = 0
    @objc dynamic var edit_date = 0
    @objc dynamic var articleImage : ArticleImage? = nil
    @objc dynamic var keywords = ""
    @objc dynamic var section = ""
    @objc dynamic var section_id = 0
    @objc dynamic var subsection = ""
    @objc dynamic var subsection_id = ""
    @objc dynamic var title = ""
    @objc dynamic var type = ""
    
    override class func primaryKey() -> String? {
        return "article"
    }
    
}
