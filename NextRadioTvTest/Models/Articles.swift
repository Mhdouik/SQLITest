//
//  Articles.swift
//  NextRadioTvTest
//
//  Created by Douik Mohamed Houcem on 18/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import RealmSwift
import SwiftyJSON
import Fakery

class Articles: Object {
    
    convenience init(data: AnyObject?) {
        self.init()
        let json = JSON(data!)
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        let faker = Faker(locale: "nb-NO")
        if let data = try? json.rawData() {
            if let articlesArray = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [[String: Any]] {
                if articlesArray != nil {
                    for i in 0 ..< (articlesArray?.count)! {
                        if let elementArray = try? articlesArray![i]["elements"] as! NSArray {
                            if let item = try? elementArray.firstObject as! NSDictionary {
                                if let articles = try? item["items"] as! NSArray {
                                    for i in 0 ..< (articles.count) {
                                        if let itemDetail = try? articles[i] as! NSDictionary {
                                            let article = Article()
                                            if let article_id = itemDetail["article"] as? Int {
                                                article.article = article_id
                                            }
                                            if let article_chapo = itemDetail["chapo"] as? String {
                                                article.chapo = article_chapo
                                            }
                                            if let article_comments = itemDetail["comments"] as? Int {
                                                article.comments = article_comments
                                            }
                                            if let article_content = itemDetail["content"] as? String {
                                                article.content = article_content
                                            }
                                            if let article_date = itemDetail["date"] as? Int {
                                                article.date = article_date
                                            }
                                            if let article_edit_date = itemDetail["edit_date"] as? Int {
                                                article.edit_date = article_edit_date
                                            }
                                            if let article_image = itemDetail["image"] as? NSDictionary {
                                                let articleImage = ArticleImage()
                                                articleImage.id = faker.number.randomInt()
                                                if let image_caption = article_image["image_caption"] as? String {
                                                    articleImage.image_caption = image_caption
                                                }
                                                if let image_copyright = article_image["image_copyright"] as? String {
                                                    articleImage.image_copyright = image_copyright
                                                }
                                                if let image_title = article_image["image_title"] as? String {
                                                    articleImage.image_title = image_title
                                                }
                                                if let image_url = article_image["image_url"] as? String {
                                                    articleImage.image_url = image_url
                                                    print(image_url)
                                                }
                                                article.articleImage = articleImage
                                            }
                                            if let article_keywords = itemDetail["keywords"] as? String {
                                                article.keywords = article_keywords
                                            }
                                            if let article_section = itemDetail["section"] as? String {
                                                article.section = article_section
                                            }
                                            if let article_section_id = itemDetail["section_id"] as? Int {
                                                article.section_id = article_section_id
                                            }
                                            if let article_subsection = itemDetail["subsection"] as? String {
                                                article.subsection = article_subsection
                                            }
                                            if let article_subsection_id = itemDetail["subsection_id"] as? String {
                                                article.subsection_id = article_subsection_id
                                            }
                                            if let article_title = itemDetail["title"] as? String {
                                                article.title = article_title
                                            }
                                            if let article_type = itemDetail["type"] as? String {
                                                article.type = article_type
                                            }
                                            try! realm.write {
                                                realm.add(article, update: true)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
