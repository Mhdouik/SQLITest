//
//  ArticleImage.swift
//  NextRadioTvTest
//
//  Created by Douik Mohamed Houcem on 18/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import RealmSwift

class ArticleImage: Object {

    @objc dynamic var id = 0
    @objc dynamic var image_caption = ""
    @objc dynamic var image_copyright = ""
    @objc dynamic var image_title = ""
    @objc dynamic var image_url = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
