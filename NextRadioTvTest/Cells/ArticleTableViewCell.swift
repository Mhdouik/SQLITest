//
//  ArticleTableViewCell.swift
//  NextRadioTvTest
//
//  Created by Douik Mohamed Houcem on 18/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var articleDescLabel: UILabel!
    @IBOutlet weak var articleDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
