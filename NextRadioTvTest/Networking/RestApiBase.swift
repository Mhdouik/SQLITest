//
//  RestApiBase.swift
//  NextRadioTvTest
//
//  Created by Douik Mohamed Houcem on 18/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

class RestApiBase {
    
    func getHeader() -> [String: String]? {
        let headers = [
            "Content-Type": "application/json"
        ]
        return headers
    }
    
}
