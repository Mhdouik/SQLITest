//
//  RestApiArticle.swift
//  NextRadioTvTest
//
//  Created by Douik Mohamed Houcem on 18/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import Alamofire

class RestApiArticle: RestApiBase {
    
    func getArticles(callback: @escaping (Articles?)->()) {
        let url = GlobalConstants.baseUrl
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding(destination: .methodDependent), headers: getHeader())
            .responseJSON { response in
                if let JSON = response.result.value {
                    callback(Articles(data: JSON as AnyObject?))
                }
        }
    }
    
}
