//
//  ArticlesListViewController.swift
//  NextRadioTvTest
//
//  Created by Douik Mohamed Houcem on 18/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import AlamofireImage

private let reuseIdentifier = "articleTableViewCell"
private let articleCellHeight = 113
private let layerCornerRadius = 9
private let articleDetailSegue = "articleDetailSegue"

class ArticlesListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var articlesList: Results<Article>! = nil
    var articlesCount = 0
    let realm = try! Realm()
    var selectItem = false
    var selectedItemIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "News"
        let restApiArticle: RestApiArticle = RestApiArticle()
        restApiArticle.getArticles(callback: {(articles) -> () in
            self.articlesList = self.realm.objects(Article.self)
            self.articlesCount = self.articlesList.count
            self.tableView.reloadData()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if selectItem {
            let nav = segue.destination as! UINavigationController
            let destinationVC = nav.topViewController as! ArticleDetailTableViewController
            destinationVC.selectedArticle = self.articlesList[self.selectedItemIndex]
        }
        selectItem = false
     }
    
}

extension ArticlesListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articlesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ArticleTableViewCell
        let imageUrl = self.articlesList[indexPath.row].articleImage?.image_url
        if imageUrl != "" {
            Alamofire.request(imageUrl!).responseImage { response in
                if let image = response.result.value {
                    cell.articleImageView.image = image
                }
            }
        }
        cell.articleImageView.layer.masksToBounds = true
        cell.articleImageView.layer.cornerRadius = CGFloat(layerCornerRadius)
        cell.articleTitleLabel.text = self.articlesList[indexPath.row].title
        cell.articleDescLabel.text = self.articlesList[indexPath.row].keywords
        // convert Int to Double
        let timeInterval = Double(self.articlesList[indexPath.row].date)
        // create NSDate from Double (NSTimeInterval)
        let articleDate = Date(timeIntervalSince1970: timeInterval)
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: articleDate)
        // convert your string to date
        let articleDateFormatter = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd/MM/yyyy"
        // again convert your date to string
        let articleDateString = formatter.string(from: articleDateFormatter!)

        cell.articleDateLabel.text = articleDateString
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(articleCellHeight)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectItem = true
        self.selectedItemIndex = indexPath.row
        performSegue(withIdentifier: "articleDetailSegue", sender: nil)
    }
}
