//
//  ArticleDetailWebViewViewController.swift
//  NextRadioTvTest
//
//  Created by Douik Mohamed Houcem on 04/03/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import UIKit
import WebKit

class ArticleDetailWebViewViewController: UIViewController {
    
    var webView: WKWebView
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Site Web"
        view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        let height = NSLayoutConstraint(item: webView, attribute: .height, relatedBy: .equal, toItem: view, attribute: .height, multiplier: 1, constant: 0)
        let width = NSLayoutConstraint(item: webView, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1, constant: 0)
        view.addConstraints([height, width])
        let url = NSURL(string:"http://www.sqli.com")
        let request = NSURLRequest(url: url! as URL)
        webView.load(request as URLRequest)
        // Do any additional setup after loading the view.
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.webView = WKWebView(frame: CGRect.zero)
        super.init(coder: aDecoder)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
